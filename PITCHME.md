---
marp: true
title: Ephemeral Development Using Gitpod
description: Gitpod Demo
theme: uncover
paginate: true
_paginate: false
---

![bg](./assets/gradient.jpg)

# <!--fit--> Ephemeral Development Using Gitpod

 Throw Away - Collaborative Development

https://gitlab.com/spiculedata/presentation

<style scoped>a { color: #eee; }</style>

<!-- This is presenter note. You can write down notes through HTML comment. -->

---

![bg](#123)
![](#fff)

##### <!--fit--> How many projects do you work on?

---

![bg](#123)
![](#fff)

##### <!--fit--> How many of those projects do you work on, on a regular basis?

---

![bg](#123)
![](#fff)

##### <!--fit--> More problematically....

##### <!--fit--> How many do you work on an irregular basis??


---

![bg](#123)
![](#fff)

##### <!--fit--> How do you keep the different development environments:

- Clean
- Up to date
- Functional

#####  ?
---

![bg](#123)
![](#fff)

##### <!--fit--> When you're collaborating how do you know you're all working off the same codebase?

---
![bg](#123)
![](#fff)

##### <!--fit--> How can we make Code Review quicker and easier?

---
![bg](#123)
![](#fff)

##### <!--fit--> How do you migrate from one development platform to another?

---

![bg](#123)
![](#fff)

##### <!--fit--> Just some of the questions we can help answer using

![width:600px](https://www.generalcatalyst.com/wp-content/uploads/2021/04/GitPod_logo_835x557_OPT-2b.png)

---
![bg](#123)
![](#fff)

##### Who uses what?

![width:600px](20220208020030.png)

---

![bg](#123)
![](#fff)

##### How Easy Is It To Get Started?

![width:200px](ss1.png)
![width:200px](ss3.png)
![width:600px](ss2.png)



---

![bg](#123)
![](#fff)

##### Lets get started...

---


